#!/bin/bash

[ -z "$1" ] && echo "empty project err" && exit

PROJ=$1
HOME=`pwd`
cd $1
mkdir -p build
cd build
cmake .. && make

#pwd && ls -l ./delife/
cp -f ./delife/delife.wasm ../../wasm-bin/
cp -f ./delife/delife.abi ../../wasm-bin/
