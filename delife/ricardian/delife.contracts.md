<h1 class="contract"> transfer </h1>

---
spec_version: "0.2.0"
title: Stake assets into contract for a benefitiary user
summary: 'Stake assets to a user but locked within the contract'
icon: @ICON_BASE_URL@/@TOKEN_ICON_URI@
---

add_admin

<h1 class="contract"> add_admin </h1>

---

del_admin

<h1 class="contract"> del_admin </h1>

---

init

<h1 class="contract"> init </h1>

---

config

<h1 class="contract"> config </h1>

---

add_project

<h1 class="contract"> add_project </h1>

---

del_project

<h1 class="contract"> del_project </h1>

---

donate

<h1 class="contract"> donate </h1>

---

<h1 class="contract"> accept </h1>

accept

---

<h1 class="contract"> showid </h1>

showid