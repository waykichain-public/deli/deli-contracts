#include "delife.hpp"
#include "delifedb.hpp"

#include <chrono>
#include <system.hpp>

using std::chrono::system_clock;
using namespace wasm;
using namespace wasm::db;
using namespace wasm::safemath;

uint64_t delife::gen_new_id(const wasm::name &counter_key, bool persist) {
    uint64_t newID = 1;
    counter_t counter(counter_key);
    if (!wasm::db::get(counter)) {
        counter.counter_val = 1;
        if (persist)
            wasm::db::set(counter);

        return 1;
    }

    counter.counter_val++;
    if (persist)
        wasm::db::set(counter);

    return counter.counter_val;
}

uint64_t delife::get_config(string conf_key) {
    config_t conf(conf_key);
    if ( !wasm::db::get(conf) ) return 0;

    return conf.conf_val;
}

void delife::set_config(string conf_key, uint64_t conf_val) {
    check(
        conf_key == CK_TOKEN_BANK_REGID
        , "conf_key invalid" );

    config_t conf = {conf_key, conf_val};

    wasm::db::set(conf);
}


uint64_t delife::inc_admin_counter(bool increase_by_one) {
    uint64_t newID = 1;
    counter_t counter(COUNTER_ADMIN);
    if (!wasm::db::get(counter)) {
        check( increase_by_one, "only increase allowed");
        counter.counter_val = 1;
        wasm::db::set(counter);

        return 1;
    }

    newID = counter.counter_val + (increase_by_one ? 1 : -1);
    counter.counter_val = newID;
    wasm::db::set(counter);

    return newID;
}

void delife::check_admin_auth(regid issuer, bool need_super_admin) {
    auto maintainer = get_maintainer(get_self());
    admin_t admin(issuer);

    if (issuer != maintainer) { //_maintainer is by default admin
        check( wasm::db::get(admin), "non-admin error" );

        if (need_super_admin)
            check( admin.is_super_admin, "super admin auth error" );

    } else {
        check( maintainer.value != 0, "maintainer disabled error" );
    }

    require_auth(issuer);
}