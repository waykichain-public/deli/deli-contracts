#pragma once
#include <wasm.hpp>
#include <asset.hpp>
#include <table.hpp>

using namespace wasm;

namespace wasm { namespace db {

#define TABLE_IN_CONTRACT [[wasm::table, wasm::contract("delife")]]

    static constexpr uint16_t DELIFE_SCOPE = 800;

    struct TABLE_IN_CONTRACT admin_t {
        regid admin;
        bool is_super_admin;

        uint64_t primary_key() const { return admin.value; }
        uint64_t       scope() const { return DELIFE_SCOPE; }

        admin_t() {}
        admin_t(regid adminIn): admin(adminIn) {}
        admin_t(regid adminIn, bool isSuperAdmin): admin(adminIn), is_super_admin(isSuperAdmin) {}

        WASMLIB_SERIALIZE( admin_t, (admin)(is_super_admin) )
    };
    typedef wasm::table<"admins"_n, admin_t, uint64_t> admin_tbl;

    //unit64 config table
    struct TABLE_IN_CONTRACT config_t {
        string     conf_key;
        uint64_t   conf_val = 0;

        string primary_key()const { return conf_key; }
        uint64_t scope()const { return DELIFE_SCOPE; }

        config_t(string k): conf_key(k) {}
        config_t(string k, uint64_t v): conf_key(k), conf_val(v) {}
        config_t() {}

        WASMLIB_SERIALIZE( config_t, (conf_key)(conf_val) )
    };
    typedef wasm::table< "config"_n, config_t, string > config_tbl;

    struct TABLE_IN_CONTRACT counter_t {
        wasm::name      counter_key;    //stake_counter | admin_counter
        uint64_t        counter_val;

        uint64_t primary_key() const { return counter_key.value; }
        uint64_t       scope() const { return DELIFE_SCOPE; }

        counter_t() {}
        counter_t(wasm::name counterKey): counter_key(counterKey) {}

        WASMLIB_SERIALIZE( counter_t, (counter_key)(counter_val) )
    };
    typedef wasm::table<"counters"_n, counter_t, uint64_t> counter_tbl;

    static constexpr uint64_t  reserved  = 0;
    enum return_t{
        NONE    = 0,
        MODIFIED,
        APPENDED,
    };

    template<typename ObjectType>
    bool get(ObjectType& object) {
        typename ObjectType::table_t objects(regid(wasm::db::reserved), object.scope());
        if ( !objects.get( object, object.primary_key())) return false;
        return true;
    }

    template<typename ObjectType>
    return_t set(const ObjectType& object) {
        ObjectType obj;
        typename ObjectType::table_t objects(regid(wasm::db::reserved), object.scope());

        if (objects.get( obj, object.primary_key() )) {
            objects.modify( obj, wasm::no_payer, [&]( auto& s ) {
               s = object;
            });
            return return_t::MODIFIED;
        } else {
            objects.emplace( wasm::no_payer, [&]( auto& s ) {
               s = object;
            });
            return return_t::APPENDED;
        }
    }

    template<typename ObjectType>
    void del(const ObjectType& object) {
        typename ObjectType::table_t objects(regid(wasm::db::reserved), object.scope());
        objects.erase(object.primary_key(), wasm::no_payer);
    }
} }