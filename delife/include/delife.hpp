#include "delifedb.hpp"

using namespace std;
using namespace wasm::db;

#define PS1( amount ) \
    amount * PRECISION_1

#define TO_ASSET( amount, code ) \
    asset(amount, symbol(code, PRECISION))

#define MINT(bank, to, quantity) \
    {wasm::transaction trx(bank, name("mint"), std::vector<permission>{{get_self(), name("wasmio_code")}}, std::tuple<regid, asset, string>(to, quantity, "mint"));\
    trx.send();}

#define BURN(bank, from, quantity) \
    {wasm::transaction trx(bank, name("burn"), std::vector<permission>{{get_self(), name("wasmio_code")}}, std::tuple<regid, asset, string>(from, quantity, "burn"));\
    trx.send();}

#define TRANSFER(bank, from, to, quantity, memo) \
    {wasm::transaction trx(bank, name("transfer"), std::vector<permission>{{from, name("wasmio_code")}}, std::tuple<regid, regid, asset, string>(from, to, quantity, memo));\
     trx.send();}

#define SHOWID(id) \
    {wasm::transaction trx(get_self(), name("showid"),std::vector<permission>{{get_self(), name("wasmio_code")}}, std::tuple<uint64_t>(id));\
     trx.send();}

#define WASM_DISPATCH_LIMIT(TYPE, MEMBERS)                                                         \
    extern "C" {                                                                                   \
    [[wasm::wasm_entry]] void apply(uint64_t receiver, uint64_t code, uint64_t action) {           \
        switch (action) {                                                                          \
            WASM_DISPATCH_HELPER(TYPE, MEMBERS)                                                    \
        default:                                                                                   \
            bool is_first_receiver = (code == receiver);                                           \
            check(!is_first_receiver, "contract '" #TYPE "' must have action '" +                  \
                                          wasm::name(action).to_string() +                         \
                                          "' when it is the first receiver");                      \
            break;                                                                                 \
        }                                                                                          \
        /* does not allow destructor of this contract to run: wasm_exit(0); */                     \
    }                                                                                              \
    }

namespace wasm { namespace safemath {
template<typename T>
uint128_t divide_decimal(uint128_t a, uint128_t b, T precision) {
    uint128_t tmp = 10 * a * precision  / b;
    return (tmp + 5) / 10;
}

template<typename T>
uint128_t multiply_decimal(uint128_t a, uint128_t b, T precision) {
    uint128_t tmp = 10 * a * b / precision;
    return (tmp + 5) / 10;
}

#define div(a, b) divide_decimal(a, b, PRECISION_1)
#define mul(a, b) multiply_decimal(a, b, PRECISION_1)

#define high_div(a, b) divide_decimal(a, b, HIGH_PRECISION_1)
#define high_mul(a,b ) multiply_decimal(a, b, HIGH_PRECISION_1)

} } //safemath

static regid wasmio_bank                                    = wasm::regid("0-800");

static constexpr int128_t HIGH_PRECISION_1                  = 100000000000000000;   //10^17
static constexpr int128_t PRECISION_1                       = 100000000;            //10^8
static constexpr int128_t PRECISION                         = 8;

static constexpr name wasmio_code                           = "wasmio_code"_n;
static constexpr name COUNTER_ADMIN                         = "admin"_n;
static constexpr name COUNTER_PROJ                          = "donproject"_n;
static constexpr name COUNTER_DONATION                      = "donation"_n;
static constexpr name COUNTER_ACCEPTION                     = "donaacept"_n;

static constexpr auto CK_TOKEN_BANK_REGID                   = "token_bank_regid";
static constexpr auto CK_TOKEN_SYMBOL                       = "token_symbol";

//mall related


CONTRACT delife : public contract
{
    using contract::contract;

    public:
        ACTION init();
        ACTION config(string conf_key, uint64_t conf_val);
        ACTION add_admin(regid issuer, regid admin, bool is_supper_admin);
        ACTION del_admin(regid issuer, regid admin);
        ACTION add_project(regid issuer, string proj_code);
        ACTION del_project(regid issuer, uint64_t proj_id);

        //charity ACTIONS
        ACTION donate(regid issuer, regid donor, uint64_t proj_id, asset quantity, string pay_sn, string memo);
        ACTION accept(regid issuer, regid to, uint64_t proj_id, asset quantity, string pay_sn, string memo);

        //mall ACTIONS
        ACTION credit(regid issuer, regid consumer, uint64_t consume_points);
        ACTION decredit(regid issuer, regid consumer, uint64_t consume_points);
        ACTION withdraw(regid issuer, regid to, asset quantity);
        ACTION invest(regid issuer, regid investor, uint64_t invest_points);
        ACTION emit_reward();

        ACTION showid(uint64_t id);

        void    pre_action();

    private:
        uint64_t    gen_new_id(const wasm::name &counter_key, bool persist = true);
        uint64_t    inc_admin_counter(bool increase_by_one = true);
        void        check_admin_auth(regid issuer, bool need_super_admin = false);
        void        set_config(string conf_key, uint64_t conf_val);
        uint64_t    get_config(string conf_key);


}; //contract delife